all: docker img-base image-sdr

clean:
	rm -f ubuntu.img

docker:
	docker pull mkaczanowski/packer-builder-arm

img-base: docker clean
	docker run \
		--rm \
		--privileged \
		-v /dev:/dev \
		-v ${PWD}:/build \
		mkaczanowski/packer-builder-arm build packer/base.json

flash-base:
	flash --force \
		--userdata ./files/boot/user-data.yaml  \
		./ubuntu-20.04.img