CA_PRIVATE_KEY="/home/rene/.ssh/ca"
SERVER_CERT_NAME="/home/rene/.ssh/server.pub"
USER_CERT_NAME="/home/rene/.ssh/zinsoldat.pub"
PRINCIPALS="node-1,node-1.local,node-2,node-2.local,node-3,node-3.local"
USERNAME=zinsoldat

# server key
ssh-keygen -h -s ${CA_PRIVATE_KEY} -n ${PRINCIPALS} -I ID ${SERVER_CERT_NAME}

# user key
ssh-keygen -s ${CA_PRIVATE_KEY} -I ${USERNAME} -n ${USERNAME} -V +52w ${USER_CERT_NAME}